package main

import "fmt"

func main() {
	l1 := &ListNode{}
	l2 := l1
	for _, ele := range []int{1, 8} {
		l2.Next = &ListNode{ele, nil}
		l2 = l2.Next
	}
	// fmt.Println(addTwoNumbers1(l1, &ListNode{Val: 0}).Next.Val)
	l3 := addTwoNumbers1(l1.Next, &ListNode{Val: 0})
	for l3 != nil {
		fmt.Println(l3.Val)
		l3 = l3.Next
	}
}

type ListNode struct {
	Val  int
	Next *ListNode
}

//  时间换空间
func addTwoNumbers1(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	head := &ListNode{}
	var longNode *ListNode = l1
	var lastNode *ListNode = head
	carry := 0
	for {
		sum := carry
		if l1 != nil {
			sum += l1.Val
		}
		if l2 != nil {
			sum += l2.Val
		}
		if sum > 9 {
			carry = 1
			sum -= 10
		} else {
			carry = 0
		}
		lastNode.Next = longNode
		lastNode = longNode
		lastNode.Val = sum
		if l1 != nil && l1.Next != nil {
			longNode = l1.Next
		} else if l2 != nil && l2.Next != nil {
			longNode = l2.Next
		} else if carry == 1 {
			longNode = &ListNode{}
		} else {
			break
		}
		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
	}
	return head.Next
}

func addTwoNumbers2(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	head := &ListNode{}
	point := head
	carry := 0

	var num int
	for l1 != nil || l2 != nil {
		num = carry
		if l1 != nil {
			num += l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			num += l2.Val
			l2 = l2.Next
		}
		if num > 9 {
			carry = 1
			num -= 10
		} else {
			carry = 0
		}
		point.Next = &ListNode{Val: num}
		point = point.Next
	}

	if carry == 1 {
		point.Next = &ListNode{Val: 1}
	}
	return head.Next
}
