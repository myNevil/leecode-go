package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(divide(10, 3))
}

func divide(dividend int, divisor int) int {
	flag := true
	if dividend < 0 {
		dividend = -dividend
		flag = !flag
	}
	if divisor < 0 {
		divisor = -divisor
		flag = !flag
	}
	if dividend < divisor {
		return 0
	}
	res := 0
	for dividend > divisor {
		temp, num := divisor, 0
		for dividend > temp<<1 {
			temp <<= 1
			num++
		}
		dividend -= temp
		res |= 1 << num
	}

	if flag {
		return min(res, math.MaxInt32)
	} else {
		return max(-res, math.MinInt32)
	}
}
